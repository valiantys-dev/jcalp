/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3rc1</a>, using an XML
 * Schema.
 * $Id$
 */

package com.valiantys.jira.plugins.createissueandlink.lnioCastor;

/**
 * Class MappingsItem.
 * 
 * @version $Revision$ $Date$
 */
public class MappingsItem implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _mapping.
     */
    private com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping _mapping;


      //----------------/
     //- Constructors -/
    //----------------/

    public MappingsItem() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'mapping'.
     * 
     * @return the value of field 'Mapping'.
     */
    public com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping getMapping(
    ) {
        return this._mapping;
    }

    /**
     * Sets the value of field 'mapping'.
     * 
     * @param mapping the value of field 'mapping'.
     */
    public void setMapping(
            final com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping mapping) {
        this._mapping = mapping;
    }

}

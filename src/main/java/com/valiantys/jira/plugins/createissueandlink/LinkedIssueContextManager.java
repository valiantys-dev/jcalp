package com.valiantys.jira.plugins.createissueandlink;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutStorageException;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.valiantys.jira.plugins.createissueandlink.business.LnioContextBean;
import com.valiantys.jira.plugins.createissueandlink.business.LnioFieldsMappingConfiguration;
import com.valiantys.jira.plugins.createissueandlink.business.LnioMappingOfFieldBean;
import com.valiantys.jira.plugins.createissueandlink.business.MappingWrapper;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.Field;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueChild;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueChildItem;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueParent;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueParentItem;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.LinkedIssueContext;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mappings;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.Param;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.ParentFieldsToUpdate;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.ParentFieldsToUpdateItem;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.SkippedFieldsItem;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.Watcher;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.WatcherToAddItem;
import com.valiantys.jira.plugins.createissueandlink.manager.LnioConfigurationManager;
import com.valiantys.jira.plugins.createissueandlink.util.LinkedIssueConstants;

/**
 * Class used to manage all contexts to apply to the new linked issues.
 * 
 * @author Maxime
 * 
 */
public final class LinkedIssueContextManager {

	/**
	 * Logger.
	 */
	private static final Logger LOG = Logger.getLogger(LinkedIssueContextManager.class);

	/**
	 * Singleton instance of this manager.
	 */
	private static LinkedIssueContextManager instance;

	/**
	 * All contexts defined in the xml configuration file.
	 */
	private LinkedIssueContext linkedIssueContext;


	/**
	 * private constructor.
	 */
	private LinkedIssueContextManager() {
	}

	/**
	 * method to retrieve the manager instance.
	 * 
	 * @return the unique instance of the context manager.
	 */
	public static LinkedIssueContextManager getInstance() {
		if (null == instance) {
			instance = new LinkedIssueContextManager();
		}
		return instance;
	}

	/**
	 * Retrieve all context by parsing the xml configuration file.
	 * 
	 * @param confXmlFileName
	 *            : xml configuration file.
	 * @return all contexts in an object structure.
	 * @throws LinkNewIssueOperationException
	 *             : exception.
	 */
	private LinkedIssueContext getLinkedIssueContext(
			final String confXmlFileName) throws LinkNewIssueOperationException {

		LOG.debug("does linkedIssueContext be null : " + linkedIssueContext);
		if (linkedIssueContext != null) {
			return linkedIssueContext;
		} else {
			Reader reader = null;
			try {

				if (ComponentManager.getComponentInstanceOfType(JiraHome.class).getHomePath() != null) {
					String jiraHomePath = ComponentManager.getComponentInstanceOfType(JiraHome.class).getHomePath(); 
					String customConfFilePath = jiraHomePath
					+ System.getProperty("file.separator") 
					+ "plugins"
					+ System.getProperty("file.separator")
					+ "lnio-conf"
					+ System.getProperty("file.separator")
					+ confXmlFileName;

					// check if a custom configuration file exists in the {JIRA_HOME} folder
					File fd = new File(customConfFilePath);
					if (fd.exists()) {

						// if it is, we load it
						reader = new BufferedReader(new InputStreamReader(new FileInputStream(customConfFilePath), Charset
								.forName("UTF-8")));
					}
				}


				if (reader == null) {

					// if it's not, we load the default configuration file contained in the JAR file
					ClassLoader loader = LinkedIssueContextManager.class.getClassLoader();
					reader = new BufferedReader(new InputStreamReader(loader
							.getResourceAsStream(confXmlFileName), Charset
							.forName("UTF-8")));

				}


				Unmarshaller unmarshaller = new Unmarshaller(
						LinkedIssueContext.class);
				linkedIssueContext = (LinkedIssueContext) unmarshaller
				.unmarshal(reader);
				reader.close();
			} catch (FileNotFoundException e) {
				LOG.error("Unable to find the configuration file : "
						+ e.getMessage());
				e.printStackTrace();
				throw new LinkNewIssueOperationException(e);
			} catch (MarshalException e) {
				LOG.error("The configuration file has a wrong format : "
						+ e.getMessage());
				e.printStackTrace();
				throw new LinkNewIssueOperationException(e);
			} catch (ValidationException e) {
				LOG.error("The configuration file is not valid : "
						+ e.getMessage());
				e.printStackTrace();
				throw new LinkNewIssueOperationException(e);
			} catch (IOException e) {
				LOG.error("LinkedIssueContextManager : IOException : "
						+ e.getMessage());
				e.printStackTrace();
				throw new LinkNewIssueOperationException(e);
			}
		}

		return linkedIssueContext;
	}





	/**
	 * Check in the configuration file if the inheritance is activated.
	 * 
	 * @return true if the inheritance is activated.
	 * @throws LinkNewIssueOperationException
	 *             : an exception.
	 */
	public boolean isLinkedIssueInheritanceActivated()
	throws LinkNewIssueOperationException {
		boolean isInheritanceActive = false;
		if (linkedIssueContext == null) {
			getLinkedIssueContext(LinkedIssueConstants.CONFIG_FILE_NAME);
		}
		String inheritanceActiveConfigValue = linkedIssueContext
		.getInheritanceActivate().toString();
		isInheritanceActive = inheritanceActiveConfigValue
		.equalsIgnoreCase(LinkedIssueConstants.TRUE);
		LOG.debug("Is inheritance global is active : " + isInheritanceActive);
		return isInheritanceActive;
	}

	/**
	 * Check in the configuration file if the inheritance is activated.
	 * 
	 * @return true if the inheritance is activated.
	 * @throws LinkNewIssueOperationException
	 *             : an exception.
	 */
	public boolean isSpecificMappingActivated(
			final LnioFieldsMappingConfiguration lnioConfig, final String idCtx)
	throws LinkNewIssueOperationException {
		boolean hasSpecificMappings = false;
		LnioContextBean contextBean = getContextObjectById(idCtx, lnioConfig);
		if (contextBean != null) {
			List l = contextBean.getListOfFieldsMapping();
			if (l != null && l.size() > 0) {
				hasSpecificMappings = true;
			}
		}
		LOG.debug("The specific fields mapping for this context : " + idCtx
				+ " is active : " + hasSpecificMappings);
		return hasSpecificMappings;
	}


	public boolean isFieldSpecificMapped(final LnioFieldsMappingConfiguration lnioConfig, final String idCtx, String fieldId)
	throws LinkNewIssueOperationException {
		boolean hasSpecificMapping = false;
		LnioContextBean contextBean = getContextObjectById(idCtx, lnioConfig);
		if (contextBean != null) {
			List l = contextBean.getListOfFieldsMapping();
			if (l != null && l.size() > 0) {
				for(int i = 0; i<l.size(); i++){
					LnioMappingOfFieldBean tmp = (LnioMappingOfFieldBean)l.get(i);
					if(fieldId!=null && fieldId.equals(tmp.getDestinationFieldId())){
						hasSpecificMapping = true;
						return hasSpecificMapping;
					}
				}

			}
		}
		return hasSpecificMapping;
	}



	public boolean isInheritanceActivatedForThisMapping(final String idCtx)	throws LinkNewIssueOperationException {
		boolean isInheritanceActive = true;
		if (linkedIssueContext == null) {
			getLinkedIssueContext(LinkedIssueConstants.CONFIG_FILE_NAME);
		}


		MappingsItem[] contexts = LinkedIssueContextManager.getInstance().getAllMappings();
		if (contexts != null && idCtx != null) {
			for (int i = 0; i < contexts.length; i++) {
				Mapping context = contexts[i].getMapping();
				if(idCtx.equals(String.valueOf(context.getId()))){
					if(context.getNoinheritance()!= null && context.getNoinheritance().length()>0 && context.getNoinheritance().equalsIgnoreCase("true")){
						isInheritanceActive = false;
						return isInheritanceActive;
					}
				}
			}
		}
		LOG.debug("is inheritance active for this mapping : " + isInheritanceActive);
		return isInheritanceActive;
	}


	/**
	 * return the object representation of a context.
	 * 
	 * @param idCtx
	 *            = context id.
	 * @return return the object representation.
	 */
	public LnioContextBean getContextObjectById(final String idCtx,
			LnioFieldsMappingConfiguration lnioConfig) {
		LnioContextBean contextBean = null;
		if (lnioConfig != null) {
			HashMap contexts = lnioConfig.getConfiguredContexts();
			if (contexts != null) {
				if (contexts.containsKey(idCtx)) {
					contextBean = (LnioContextBean) contexts.get(idCtx);
				}
			}
		}
		return contextBean;
	}

	/**
	 * Check in the configuration file if the multi link is activated.
	 * 
	 * @return true if the contextualization is activated.
	 * @throws LinkNewIssueOperationException
	 *             : an exception.
	 */
	public boolean isMultiLinkActivated() throws LinkNewIssueOperationException {
		boolean isMultiLinkActive = false;
		if (linkedIssueContext == null) {
			getLinkedIssueContext(LinkedIssueConstants.CONFIG_FILE_NAME);
		}
		String multiLinkValue = linkedIssueContext.getMultiLinkActivate()
		.toString();
		isMultiLinkActive = multiLinkValue.equalsIgnoreCase(LinkedIssueConstants.TRUE);
		return isMultiLinkActive;
	}

	/**
	 * Check in the configuration file if the multi link is activated.
	 * 
	 * @return true if the contextualization is activated.
	 * @throws LinkNewIssueOperationException
	 *             : an exception.
	 */
	public boolean isDefaultLinkActivated() throws LinkNewIssueOperationException {
		boolean isDefaultLinkActivate = false;
		if (linkedIssueContext == null) {
			getLinkedIssueContext(LinkedIssueConstants.CONFIG_FILE_NAME);
		}
		String defaultLinkValue = linkedIssueContext.getDefaultLinkActivate().toString();
		isDefaultLinkActivate = defaultLinkValue.equalsIgnoreCase(LinkedIssueConstants.TRUE);
		return isDefaultLinkActivate;
	}

	/**
	 * Check in the configuration file if the link displaying parameter is activated.
	 * 
	 * @return true displaying link parameter is activated.
	 * @throws LinkNewIssueOperationException
	 *             : an exception.
	 */
	public boolean isLinkDisplayingActivated() throws LinkNewIssueOperationException {
		boolean isLinkDisplayingActivate = false;
		if (linkedIssueContext == null) {
			getLinkedIssueContext(LinkedIssueConstants.CONFIG_FILE_NAME);
		}
		String displayingLinkValue = linkedIssueContext.getLinkDisplayingActivate().toString();
		isLinkDisplayingActivate = displayingLinkValue.equalsIgnoreCase(LinkedIssueConstants.TRUE);
		return isLinkDisplayingActivate;
	}

	/**
	 * 
	 * @param parentIssue
	 *            : The current issue.
	 * @param mappingId
	 *            :
	 * @return true is the
	 * @throws LinkNewIssueOperationException
	 */
	public boolean isIssueMapped(Issue parentIssue, String mappingId)
	throws LinkNewIssueOperationException {
		if (linkedIssueContext == null) {
			getLinkedIssueContext(LinkedIssueConstants.CONFIG_FILE_NAME);
		}
		Mapping map = getMappingByIssueParent(parentIssue, mappingId);
		return !(map == null);
	}

	/**
	 * get the mapping for the issue parentIssue according to the mappingId if
	 * mappingId is not null, return the mapping of id mappingId, get the
	 * mapping matching with the parentIssue or null if no mapping match.
	 * 
	 * @param parentIssue
	 * @param mappingId
	 * @return
	 * @throws LinkNewIssueOperationException
	 */
	private Mapping getMappingByIssueParent(Issue parentIssue, String mappingId)
	throws LinkNewIssueOperationException {
		Mapping mapping = null;

		// if mappingId is empty, we're working with the default issue link, so there's no mapping defined
		if ( (mappingId != null) && (mappingId.compareTo("") != 0) ) {

			if (linkedIssueContext == null) {
				getLinkedIssueContext(LinkedIssueConstants.CONFIG_FILE_NAME);
			}

			if (mappingId == null) {
				return null;
			}

			Mappings allMappings = linkedIssueContext.getMappings();

			if (allMappings != null) {
				MappingsItem[] mappinsItems = allMappings.getMappingsItem();

				if (mappinsItems != null && mappinsItems.length > 0) {
					int cpt = 0;
					int matchingLevel;
					while (cpt < mappinsItems.length) {
						Mapping aMapping = mappinsItems[cpt].getMapping();

						matchingLevel = checkMatchingIssue(aMapping.getIssueParent(), parentIssue);

						if (matchingLevel > 0) {
							if (mapping != null) {
								if (mapping.getIssueParent().getIssueParentItem().length < aMapping
										.getIssueParent().getIssueParentItem().length) {
									mapping = aMapping;
								}
							} else {
								if (aMapping.getId() == Long.valueOf(mappingId)) {
									mapping = aMapping;
								}
							}
						}
						cpt++;
					}
				}
			}

		}

		return mapping;
	}

	/**
	 * get the mapping for the issue parentIssue according to the mappingId if
	 * mappingId is not null, return the mapping of id mappingId, get the
	 * mapping matching with the parentIssue or null if no mapping match.
	 * 
	 * @param parentIssue
	 * @param mappingId
	 * @return
	 * @throws LinkNewIssueOperationException
	 */
	private Mapping getMappingByIssueParentDoDefault(Issue parentIssue, String mappingId)
	throws LinkNewIssueOperationException {
		Mapping mapping = null;

		// if mappingId is empty, we're working with the default issue link, so there's no mapping defined
		if ( mappingId != null && mappingId.compareTo("") != 0 ) {

			if (linkedIssueContext == null) {
				getLinkedIssueContext(LinkedIssueConstants.CONFIG_FILE_NAME);
			}

			if (mappingId == null) {
				return null;
			}

			Mappings allMappings = linkedIssueContext.getMappings();

			if (allMappings != null) {

				MappingsItem[] mappinsItems = allMappings.getMappingsItem();

				if (mappinsItems != null && mappinsItems.length > 0) {

					int cpt = 0;

					boolean mappingFound = false;

					while (cpt < mappinsItems.length && !mappingFound) {

						Mapping aMapping = mappinsItems[cpt].getMapping();

						if (aMapping.getId() == Long.valueOf(mappingId)) {
							mapping = aMapping;
							mappingFound = true;
						}	
						cpt++;
					}
				}
			}
		}

		return mapping;
	}

	/**
	 * 
	 * @param parentIssue
	 * @return
	 */
	public MappingsItem[] getAllMappings()
	throws LinkNewIssueOperationException {
		ArrayList mappings = null;
		if (linkedIssueContext == null) {
			getLinkedIssueContext(LinkedIssueConstants.CONFIG_FILE_NAME);
		}
		Mappings allMappings = linkedIssueContext.getMappings();
		return allMappings.getMappingsItem();
	}

	/**
	 * 
	 * @param parentIssue
	 * @return
	 */
	public Mapping[] getRankedMappingsByIssueParent(Issue parentIssue)
	throws LinkNewIssueOperationException {

		int matchingLevel = 0; /* niveau de matching entre l'issue courante, et l'issue configur�e
		 * via le fichier de configuration 
		 */

		// variables de comptage pour le tri du tableau de mappings
		int i = 0;


		// r�cup�ration de la configuration de LNIO ( "linkedIssue-context.xml" )
		ArrayList mappings = new ArrayList();
		if (linkedIssueContext == null) {
			getLinkedIssueContext(LinkedIssueConstants.CONFIG_FILE_NAME);
		}

		// r�cup�ration du tableau de mappings qui matchent avec l'issue courante
		Mappings allMappings = linkedIssueContext.getMappings();
		if (allMappings != null) {
			MappingsItem[] mappinsItems = allMappings.getMappingsItem();
			if (mappinsItems != null && mappinsItems.length > 0) {
				int cpt = 0;
				while (cpt < mappinsItems.length) {
					Mapping aMapping = mappinsItems[cpt].getMapping();
					matchingLevel = checkMatchingIssue(aMapping
							.getIssueParent(), parentIssue);
					if (matchingLevel > 0) {
						mappings.add(aMapping);
					}
					cpt++;
				}
			}
		}

		//--------------------------------------------------------------//
		// TRI DU TABLEAU DE MAPPING PAR NIVEAU DECROISSANT DE MATCHING //
		//--------------------------------------------------------------//
		/* On dispose maintenant d'un tableau de mappings qui matchent avec l'issue courante. Il faut
		 * maintenant le trier par ordre d�croissant de "matchingLevel" pour n'afficher que les
		 * mappings les plus fid�les.
		 */

		// le tableau de mappings tri� par ordre d�croissant de matchingLevel
		Mapping[] mappingsRankedByMatchingLevel = new Mapping[mappings.size()];

		int currentRankIndex = 0;
		int maxMatchingLevel;
		int currentMatchingLevel;


		// Tant qu'on a pas cat�goris� tous les mappings de la liste
		while (mappings.size() > 0) {

			// on r�cup�re la valeur maximum des matchingLevel de la liste non-tri�e
			maxMatchingLevel = 0;
			for (i = 0; i < mappings.size(); i++) {

				currentMatchingLevel = checkMatchingIssue( ((Mapping) mappings.get(i)).getIssueParent(), parentIssue);

				// si le matchingLevel est plus grande que la valeur max courante, on la garde
				if ( currentMatchingLevel > maxMatchingLevel) {
					maxMatchingLevel = currentMatchingLevel;
				}
			}

			/* maintenant qu'on a la valeur max de matchingLevel, on va mettre tous les mappings qui ont
			 * ce matchingLevel � la suite du tableau tri�
			 */
			i = 0;
			while (i < mappings.size()) {

				currentMatchingLevel = checkMatchingIssue( ((Mapping) mappings.get(i)).getIssueParent(), parentIssue);

				// si le matchingLevel du mapping en cours vaut la valeur max des matchingLevel, on le rajoute
				if ( currentMatchingLevel == maxMatchingLevel ) {
					mappingsRankedByMatchingLevel[currentRankIndex] = (Mapping) mappings.get(i);
					currentRankIndex++;

					// on supprime le mapping de la liste non-tri�e puisqu'il a �t� trait�
					mappings.remove(i);

					/* on n'incr�mente pas la valeur de i, puisque la suppression de la liste va
					 * entrainer un shift vers la gauche de tous les elements apr�s celui supprim�,
					 * donc il ne faut pas incr�menter la position sinon le mapping qui remplace 
					 * celui supprim� � la position i ne sera pas trait� 
					 */

				}else{

					// on incr�mente la valeur de i pour traiter la case suivante
					i++;

				}

			}

		}

		return mappingsRankedByMatchingLevel;
	}

	/**
	 * 
	 * 
	 * @param issueConfigured
	 * @param parentIssue
	 * @return
	 */
	public int checkMatchingIssue(IssueParent issueConfigured,
			Issue parentIssue) {
		int matchingLevel = 0;
		boolean conflictBetweenValuesExists = false;

		if (issueConfigured != null && parentIssue != null) {
			IssueParentItem[] issueParentItems = issueConfigured.getIssueParentItem();
			if (issueParentItems != null && issueParentItems.length > 0) {
				String key;
				String valueConfig = "";
				String valueIssue = "";

				for (int i = 0; i < issueParentItems.length; i++) {
					Param parameter = issueParentItems[i].getParam();
					key = parameter.getKey();
					valueConfig = parameter.getValue();

					if (key != null && valueConfig != null
							&& valueConfig.length() > 0) {

						// if this key is linked to the issue status
						if (key.equals(LinkedIssueConstants.ID_STATUS)) {
							valueIssue = parentIssue.getStatusObject().getId();

							/* if it's matching, the matching level is increased. Else, there's a 
							 * matching conflict. The mapping has to be forgotten
							 */
							if (valueIssue.equals(valueConfig)) {
								matchingLevel += 1;
							}else{
								conflictBetweenValuesExists = true;
							}

							// if this key is linked to the issue type
						} else if (key.equals(LinkedIssueConstants.ID_TYPE)) {
							valueIssue = parentIssue.getIssueTypeObject()
							.getId();

							/* if it's matching, the matching level is increased. Else, there's a 
							 * matching conflict. The mapping has to be forgotten
							 */
							if (valueIssue.equals(valueConfig)) {
								matchingLevel += 1;
							}else{
								conflictBetweenValuesExists = true;
							}

							// if this key is linked to the issue project
						} else if (key.equals(LinkedIssueConstants.ID_PROJECT)) {
							valueIssue = String.valueOf(parentIssue
									.getProjectObject().getId());

							/* if it's matching, the matching level is increased. Else, there's a 
							 * matching conflict. The mapping has to be forgotten
							 */
							if (valueIssue.equals(valueConfig)) {
								matchingLevel += 1;
							}else{
								conflictBetweenValuesExists = true;
							}
						} else {
							valueConfig = "";
							LOG
							.warn("This param key : "
									+ key
									+ " is not treated in this version of the plugin");
						}
					}
				}
			}
		}

		/* if a value defined in the mapping is different in conflict with the value defined 
		 * in the current issue, the mapping has to be forgotten, so the matching level is set to 0.
		 */
		if (conflictBetweenValuesExists) {
			matchingLevel = 0;
		}

		return matchingLevel;
	}



	/**
	 * Return the child issue (parameters) by the parent Issue.
	 * 
	 * @param parentIssue
	 * @return
	 */
	private IssueChild getIssueChildByIssueParent(Issue parentIssue,
			String mappingId) throws LinkNewIssueOperationException {
		IssueChild childIssue = null;
		Mapping mapping = getMappingByIssueParent(parentIssue, mappingId);
		if (mapping != null) {
			childIssue = mapping.getIssueChild();
		}
		LOG.debug("Retrieve this childIssue : " + childIssue
				+ " by this parent Issue :" + parentIssue);
		return childIssue;
	}


	/**
	 * Return the child issue (parameters) by the parent Issue.
	 * 
	 * @param parentIssue
	 * @return
	 */
	private IssueChild getIssueChildByIssueParentDoDefault(Issue parentIssue,
			String mappingId) throws LinkNewIssueOperationException {
		IssueChild childIssue = null;
		Mapping mapping = getMappingByIssueParentDoDefault(parentIssue, mappingId);
		if (mapping != null) {
			childIssue = mapping.getIssueChild();
		}
		LOG.debug("Retrieve this childIssue : " + childIssue
				+ " by this parent Issue :" + parentIssue);
		return childIssue;
	}

	private String getParamValueByChildIssueAndParamKey(IssueChild childIssue,
			String paramKey) {
		String paramValue = null;
		if (paramKey != null && childIssue != null) {
			IssueChildItem[] issueChildItems = childIssue.getIssueChildItem();
			if (issueChildItems != null && issueChildItems.length > 0) {
				for (int i = 0; i < issueChildItems.length; i++) {
					Param parameter = issueChildItems[i].getParam();
					if (paramKey.equals(parameter.getKey())) {
						return parameter.getValue();
					}
				}
			}
		}
		return paramValue;
	}

	public String getLinkDescription(Issue parentIssue, String mappingId)
	throws LinkNewIssueOperationException {
		String linkTypeName = null;
		IssueChild childIssue = getIssueChildByIssueParent(parentIssue,
				mappingId);
		linkTypeName = getParamValueByChildIssueAndParamKey(childIssue,
				LinkedIssueConstants.LINK_DESCRIPTION);
		return linkTypeName;
	}

	public String getIssueTypeId(Issue parentIssue, String mappingId)
	throws LinkNewIssueOperationException {
		String issueTypeId = null;
		IssueChild childIssue = getIssueChildByIssueParent(parentIssue,
				mappingId);
		issueTypeId = getParamValueByChildIssueAndParamKey(childIssue,
				LinkedIssueConstants.ID_TYPE);
		return issueTypeId;
	}
	
	public String getIssueTypeIdDoDefault(Issue parentIssue, String mappingId)
	throws LinkNewIssueOperationException {
		String issueTypeId = null;
		IssueChild childIssue = getIssueChildByIssueParentDoDefault(parentIssue,
				mappingId);
		issueTypeId = getParamValueByChildIssueAndParamKey(childIssue,
				LinkedIssueConstants.ID_TYPE);
		return issueTypeId;
	}

	public String getProjectId(Issue parentIssue, String mappingId)
	throws LinkNewIssueOperationException {
		String projectId = null;
		IssueChild childIssue = getIssueChildByIssueParent(parentIssue,
				mappingId);
		projectId = getParamValueByChildIssueAndParamKey(childIssue,
				LinkedIssueConstants.ID_PROJECT);
		if (projectId == null) {
			projectId = String.valueOf(parentIssue.getProjectObject().getId());
		}

		return projectId;
	}

	public String getJIRASubtaskValue(Issue parentIssue, String mappingId)
	throws LinkNewIssueOperationException {
		String jiraSubTaskFlagValue = null;
		IssueChild childIssue = getIssueChildByIssueParentDoDefault(parentIssue,
				mappingId);
		jiraSubTaskFlagValue = getParamValueByChildIssueAndParamKey(childIssue,
				LinkedIssueConstants.JIRA_SUB_TASK_FLAG);
		return jiraSubTaskFlagValue;
	}

	public String getProjectDefinedForChildIssueInMappingOfParentIssue(
			Issue parentIssue, String mappingId)
	throws LinkNewIssueOperationException {
		if (linkedIssueContext == null) {
			getLinkedIssueContext(LinkedIssueConstants.CONFIG_FILE_NAME);
		}
		Mapping map = getMappingByIssueParent(parentIssue, mappingId);
		if (map != null) {
			IssueChildItem[] issueChildItems = map.getIssueChild()
			.getIssueChildItem();
			if (issueChildItems != null && issueChildItems.length > 0) {
				String key;
				String value;
				for (int i = 0; i < issueChildItems.length; i++) {
					Param parameter = issueChildItems[i].getParam();
					key = parameter.getKey();
					value = parameter.getValue();
					if (key.equals(LinkedIssueConstants.ID_PROJECT)
							&& key != null && value != null
							&& value.length() > 0) {
						return value;
					}
				}
				LOG
				.warn("Issue Child Project not defined in mapping of parent issue");
			}
		}
		return null;
	}

	public boolean areFieldsToSkip(Issue parentIssue, String mappingId)
	throws LinkNewIssueOperationException {
		Mapping mapping = getMappingByIssueParent(parentIssue, mappingId);
		boolean areFieldsToSkip = false;
		if (mapping != null) {
			areFieldsToSkip = !(mapping.getSkippedFields() == null);
		}
		LOG.debug("areFieldsToSkip = " + areFieldsToSkip);
		return areFieldsToSkip;
	}

	public String getSkippedFieldId(Field field)
	throws LinkNewIssueOperationException {
		String id = null;
		if (field.getId() != null && field.getId().length() > 0) {
			id = field.getId();
		}
		return id;
	}

	public String getSkippedFieldName(Field field)
	throws LinkNewIssueOperationException {
		String name = null;
		if (field.getName() != null && field.getName().length() > 0) {
			name = field.getName();
		}
		return name;
	}

	public SkippedFieldsItem[] getSkippedFieldsItems(Issue parentIssue,
			String mappingId) throws LinkNewIssueOperationException {
		Mapping mapping = getMappingByIssueParent(parentIssue, mappingId);
		SkippedFieldsItem[] skippedFieldsItems = null;
		if (mapping != null) {
			if (mapping.getSkippedFields() != null) {
				skippedFieldsItems = mapping.getSkippedFields()
				.getSkippedFieldsItem();
			}
		}
		return skippedFieldsItems;
	}

	/**
	 * This method check in the configuration if the passed mapping has to duplicate the original issue attachments. 
	 * @param parentIssue : the parent issue.
	 * @param mappingId : the context id.
	 * @return true if we had to duplicate the attachments.
	 * @throws LinkNewIssueOperationException : LinkNewIssueOperationException.
	 */
	public boolean isDuplicateAttachmentsActivated(final Issue parentIssue, final String mappingId) throws LinkNewIssueOperationException {
		boolean isDuplicateAttachActivated = false;

		LOG.debug("parentIssue = " + parentIssue);
		LOG.debug("mapping.mappingId = " + mappingId);
		// retrieve mapping
		Mapping mapping = getMappingByIssueParent(parentIssue, mappingId);

		if (mapping != null && mapping.getDuplicateAttachements() != null) {
			isDuplicateAttachActivated = mapping.getDuplicateAttachements().equalsIgnoreCase("true");
		}
		LOG.debug("Attachements are to be duplicated = " + isDuplicateAttachActivated);
		return isDuplicateAttachActivated;
	}

	public boolean AreParentFieldsToUpdate(Issue parentIssue, String mappingId)
	throws LinkNewIssueOperationException {
		boolean areParentFieldsToUpdate = false;
		Mapping mapping = getMappingByIssueParent(parentIssue, mappingId);
		if (mapping != null) {
			areParentFieldsToUpdate = !(mapping.getParentFieldsToUpdate() == null);
		}
		LOG.debug("areParentFieldsToUpdate = " + areParentFieldsToUpdate);
		return areParentFieldsToUpdate;
	}

	public HashMap getParentCustomFieldsIdsToUpdate(Issue parentIssue,
			String mappingId) {
		Mapping mapping = null;
		HashMap cfIds = null;
		try {
			mapping = getMappingByIssueParent(parentIssue, mappingId);
		} catch (LinkNewIssueOperationException e) {
			LOG.error(e.getMessage());
		}
		if (mapping != null) {
			ParentFieldsToUpdate allFields = mapping.getParentFieldsToUpdate();
			if (allFields != null) {
				ParentFieldsToUpdateItem[] allFieldsItems = allFields
				.getParentFieldsToUpdateItem();
				if (allFieldsItems != null && allFieldsItems.length > 0) {
					Field field = null;
					for (int i = 0; i < allFieldsItems.length; i++) {
						field = allFieldsItems[i].getField();
						FieldManager fm = (FieldManager) ComponentManager
						.getComponentInstanceOfType(FieldManager.class);
						if (field.getId() != null) {
							if (fm
									.getCustomField(FieldManager.CUSTOM_FIELD_PREFIX
											+ field.getId()) != null) {
								if (cfIds == null) {
									cfIds = new HashMap();
								}
								cfIds.put(FieldManager.CUSTOM_FIELD_PREFIX
										+ field.getId(), field.getValue());
								LOG.debug("Field To update : "
										+ FieldManager.CUSTOM_FIELD_PREFIX
										+ field.getId());
							} else {
								LOG.error("wrong customfield id : "
										+ field.getId());
							}
						}
					}
				} else {
					LOG
					.debug("unable to get parent cf to update : parentFieldsToUpdate have not Items");
				}
			} else {
				LOG
				.debug("unable to get parent cf to update : parentFieldsToUpdate are not defined");
			}
		} else {
			LOG.debug("unable to get parent cf to update : no mapping found");
		}
		return cfIds;
	}

	public HashMap getParentFieldsToUpdate(Issue parentIssue, String mappingId) {
		Mapping mapping = null;
		HashMap fieldIds = null;
		try {
			mapping = getMappingByIssueParent(parentIssue, mappingId);
		} catch (LinkNewIssueOperationException e) {
			LOG.error(e.getMessage());
		}
		if (mapping != null) {
			ParentFieldsToUpdate allFields = mapping.getParentFieldsToUpdate();
			if (allFields != null) {
				ParentFieldsToUpdateItem[] allFieldsItems = allFields
				.getParentFieldsToUpdateItem();
				if (allFieldsItems != null && allFieldsItems.length > 0) {
					Field field = null;
					for (int i = 0; i < allFieldsItems.length; i++) {
						field = allFieldsItems[i].getField();
						FieldManager fm = (FieldManager) ComponentManager
						.getComponentInstanceOfType(FieldManager.class);
						if (field.getName() != null) {
							if (fm.getField(field.getName()) != null) {
								if (fieldIds == null) {
									fieldIds = new HashMap();
								}
								fieldIds.put(field.getName(), field.getValue());
							} else {
								LOG
								.error("wrong field id : "
										+ field.getName());
							}
						}
					}
				} else {
					LOG
					.debug("unable to get parent fields to update : parentFieldsToUpdate have not Items");
				}
			} else {
				LOG
				.debug("unable to get parent cf to update : parentFieldsToUpdate are not defined");
			}
		} else {
			LOG.debug("unable to get parent cf to update : no mapping found");
		}
		return fieldIds;
	}

	public boolean isWatcherToAdd(Issue parentIssue, String mappingId)
	throws LinkNewIssueOperationException {
		boolean isWatcherToAdd = false;
		Mapping mapping = LinkedIssueContextManager.getInstance()
		.getMappingByIssueParent(parentIssue, mappingId);
		if (mapping != null) {
			isWatcherToAdd = !(mapping.getWatcherToAdd() == null);
		}
		LOG.debug("isWatcherToAdd = " + isWatcherToAdd);
		return isWatcherToAdd;
	}

	public WatcherToAddItem[] getWatcherToAddItems(Issue parentIssue,
			String mappingId) throws LinkNewIssueOperationException {
		Mapping mapping = LinkedIssueContextManager.getInstance()
		.getMappingByIssueParent(parentIssue, mappingId);
		WatcherToAddItem[] watcherToAddItems = null;
		if (mapping != null) {
			if (mapping.getWatcherToAdd() != null)
				watcherToAddItems = mapping.getWatcherToAdd()
				.getWatcherToAddItem();
		}
		return watcherToAddItems;
	}

	public String getWatcherToAddRole(Watcher watcher)
	throws LinkNewIssueOperationException {
		String role = null;
		if (watcher.getRole() != null && watcher.getRole().length() > 0) {
			role = watcher.getRole();
		}
		return role;
	}

	public String getWatcherToAddCfId(Watcher watcher)
	throws LinkNewIssueOperationException {
		String CfId = null;
		if (watcher.getCustomFieldId() != null
				&& watcher.getCustomFieldId().length() > 0) {
			CfId = watcher.getCustomFieldId();
		}
		return CfId;
	}

	public Mapping getBestMatchingIssue(Mapping[] mappings) {
		Mapping bestMapping = null;
		Mapping tmp = null;

		if (mappings != null && mappings.length > 0) {
			for (int i = 0; i < mappings.length; i++) {
				tmp = mappings[i];
				if(tmp.getIsAContext().equalsIgnoreCase("true")){
					if (bestMapping == null) {
						bestMapping = tmp;
					} else {
						if (bestMapping.getIssueParent().getIssueParentItemCount() < tmp
								.getIssueParent().getIssueParentItemCount()) {
							bestMapping = tmp;
						}
					}
				}
			}
		}
		return bestMapping;
	}


	/**
	 * This function search and return the configured mappings which are
	 * entirely child defined (3 parameters defined : ISSUE_TYPE & ISSUE_STATUS & PROJECT)
	 * @param lic : the LinkedIssueContext object (LNIO configuration object) 
	 * @return the list of entirely child defined mappings
	 */
	public List<Mapping> getEntirelyChildDefinedMappings() throws LinkNewIssueOperationException {

		List<Mapping> entirelyChildIssueDefinedMappings = new ArrayList<Mapping>();

		// retrieval of LNIO configuration object
		if (linkedIssueContext == null) {
			linkedIssueContext = this.getLinkedIssueContext(LinkedIssueConstants.CONFIG_FILE_NAME);
		}

		// Retrieval of the entire mappings list
		Mappings mappingList = linkedIssueContext.getMappings();

		// for all mappings defined,
		for (int i = 0; i < mappingList.getMappingsItemCount(); i++) {

			// if it is entirely "issueChild" defined,
			if ( new MappingWrapper(mappingList.getMappingsItem(i).getMapping()).isEntirelydChildDefined() ) {

				// we keep it in the list to be returned
				entirelyChildIssueDefinedMappings.add(mappingList.getMappingsItem(i).getMapping());

			}

		}

		return (entirelyChildIssueDefinedMappings);

	}

	/**
	 * 
	 * @param mappingId : the mapping id which is looked for
	 * @return The mapping object found, or null if the mapping id doesn't exist
	 * @throws LinkNewIssueOperationException
	 */
	public Mapping getMappingById(String mappingId) throws LinkNewIssueOperationException {

		MappingsItem[] allMappings = this.getAllMappings();

		for (int i = 0; i<allMappings.length; i++) {
			if (String.valueOf(allMappings[i].getMapping().getId()).compareTo(mappingId) == 0) {
				return allMappings[i].getMapping();
			}
		}

		return null;

	}


	/**
	 * This method search for all issues linked to the parent issue given in parameter
	 * @param parentIssue
	 * @return a list containing all issues linked to the parent issue
	 */
	public List<Issue> getExistingLinkedIssues(Issue parentIssue, Mapping mapping) {

		long sourceId;
		long destinationId;
		Issue linkedIssue;
		List<Issue> existingLinkedIssues = new ArrayList<Issue>();
		IssueLink issueLinkTemp = null;
		String linkDirection = "";
		String mappingLinkDescription = "";
		IssueChildItem[] issueChildParams;
		IssueLinkType issueLinkTypeTemp = null;
		boolean linkTypeFound = false;

		try {

			issueChildParams = mapping.getIssueChild().getIssueChildItem();
			Collection<IssueLinkType> linkTypes = ComponentManager.getComponentInstanceOfType(IssueLinkTypeManager.class).getIssueLinkTypes();

			for (int i = 0; i < issueChildParams.length; i++) {

				if ( issueChildParams[i].getParam().getKey().compareTo(LinkedIssueConstants.LINK_DESCRIPTION) == 0 ) {

					mappingLinkDescription = issueChildParams[i].getParam().getValue();

				}

			}

			Iterator linkTypesIt = linkTypes.iterator();

			while ( linkTypesIt.hasNext() && !linkTypeFound ) {

				issueLinkTypeTemp = (IssueLinkType) linkTypesIt.next();
				if (issueLinkTypeTemp.getOutward().compareTo(mappingLinkDescription) == 0) {
					linkDirection = LinkedIssueConstants.OUTWARD;
					linkTypeFound = true;
				}

				if (issueLinkTypeTemp.getInward().compareTo(mappingLinkDescription) == 0) {
					linkDirection = LinkedIssueConstants.INWARD;
					linkTypeFound = true;
				}

			}



			if (linkDirection.compareTo(LinkedIssueConstants.OUTWARD) == 0) {

				List<IssueLink> inwardLinks = ComponentManager.getInstance().getIssueLinkManager().getInwardLinks(parentIssue.getId());

				if (inwardLinks != null) {
					Iterator inIterator = inwardLinks.iterator();

					while (inIterator.hasNext()) {

						LOG.debug("An inward link");

						issueLinkTemp = (IssueLink) inIterator.next();
						sourceId = issueLinkTemp.getSourceId();
						destinationId = issueLinkTemp.getDestinationId();

						linkedIssue = ManagerFactory.getIssueManager().getIssueObject(sourceId);

						LOG.debug("linkedIssue summary : " + linkedIssue.getSummary());

						existingLinkedIssues.add(linkedIssue);

					}
				}else{
					LOG.debug("There's no inward linked issues linked to the parent issue");
				}

			}else{

				List<IssueLink> outwardLinks = ComponentManager.getInstance().getIssueLinkManager().getOutwardLinks(parentIssue.getId());

				if (outwardLinks != null) {
					Iterator outIterator = outwardLinks.iterator();

					LOG.debug("An outward link");

					while (outIterator.hasNext()) {

						issueLinkTemp = (IssueLink) outIterator.next();
						sourceId = issueLinkTemp.getSourceId();
						destinationId = issueLinkTemp.getDestinationId();

						linkedIssue = ManagerFactory.getIssueManager().getIssueObject(destinationId);

						LOG.debug("linkedIssue summary : " + linkedIssue.getSummary());

						existingLinkedIssues.add(linkedIssue);

					}
				}else{
					LOG.debug("There's no outward linked issues linked to the parent issue");
				}

			}



		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (!linkTypeFound) {
			LOG.warn("The link description defined in the mapping doesn't exist. So the issue isn't created");
		}

		return existingLinkedIssues;

	}

	/**
	 * This method checks if already exists linked issue to the parent issue, which have exactly the same child 
	 * issue scheme than the mapping defined in the LNIO configuration file
	 * @param existingLinkedIssues : the list of already linked issues to the parent issue (only with the same link type and direction)
	 * @param mapping : the mapping object defined in the cnofiguration file
	 * @return
	 */
	public boolean existsLinkedIssuesWithSameIssueChildScheme(Issue parentIssue, List<Issue> existingLinkedIssues, Mapping mapping) {

		boolean existsLinkedIssuesWithSameIssueChildScheme = false;
		int matchingLevel = 0;
		Issue issueTemp;
		IssueChildItem[] issueChildParams = mapping.getIssueChild().getIssueChildItem();
		int i = 0;
		Collection<IssueLinkType> linkTypes = ComponentManager.getComponentInstanceOfType(IssueLinkTypeManager.class).getIssueLinkTypes();
		Iterator linkTypesIt;
		IssueLinkType issueLinkTypeTemp;


		Iterator it = existingLinkedIssues.iterator();
		while ( it.hasNext() && !existsLinkedIssuesWithSameIssueChildScheme ) {

			issueTemp = (Issue) it.next();

			for (i = 0; i < issueChildParams.length; i++) {

				if ( issueChildParams[i].getParam().getKey().compareTo(LinkedIssueConstants.ID_TYPE) == 0 ) {
					if ( issueChildParams[i].getParam().getValue().compareTo(issueTemp.getIssueTypeObject().getId()) == 0 ) {
						matchingLevel++;
					}
				}

				if ( issueChildParams[i].getParam().getKey().compareTo(LinkedIssueConstants.ID_PROJECT) == 0 ) {
					if ( issueChildParams[i].getParam().getValue().compareTo("CURRENT_PRJ") != 0) {
						if ( issueChildParams[i].getParam().getValue().compareTo(issueTemp.getProjectObject().getId().toString()) == 0 ) {
							matchingLevel++;
						}
					}else{
						if ( parentIssue.getProjectObject().getId().toString().compareTo(issueTemp.getProjectObject().getId().toString()) == 0 ) {
							matchingLevel++;
						}
					}
				}

				if ( issueChildParams[i].getParam().getKey().compareTo(LinkedIssueConstants.LINK_DESCRIPTION) == 0 ) {

					linkTypesIt = linkTypes.iterator();
					while (linkTypesIt.hasNext()) {

						issueLinkTypeTemp = (IssueLinkType) linkTypesIt.next();
						if ( (issueChildParams[i].getParam().getValue().compareTo(issueLinkTypeTemp.getOutward()) == 0)
								|| (issueChildParams[i].getParam().getValue().compareTo(issueLinkTypeTemp.getInward()) == 0) ) {
							matchingLevel++;

						}

					}
				}

			}

			if (matchingLevel==3) {
				existsLinkedIssuesWithSameIssueChildScheme = true;
			}else{
				matchingLevel = 0;
			}

		}

		return existsLinkedIssuesWithSameIssueChildScheme; 
	}


	/**
	 * This method checks if exists mandatory fields defined for this project and the child issue type,
	 * which are not mapped by the field by field mapping configuration
	 * 
	 * @param lnioConfigManager : the field by field configuration manager
	 * @param mapping : the mapping object defined in the configuration file
	 * 
	 * @return true if exists some mandatory fields defined for this project and the child issue type,
	 * which are not mapped by the field by field mapping configuration, false otherwise
	 * 
	 * @throws FieldLayoutStorageException
	 */
	public boolean existsRequiredFieldsNotMapped(LnioConfigurationManager lnioConfigManager, 
			Mapping mapping, Issue parentIssue) throws FieldLayoutStorageException{

		LnioMappingOfFieldBean fieldMappingTemp;
		Project projectTemp;
		boolean fieldMappingFound = false;
		int i;
		String issueChildProjectId = "";



		ProjectManager pm = ComponentManager.getInstance().getProjectManager();
		List<Project> projectList = pm.getProjectObjects();

		if (projectList != null) {

			LOG.debug("ICI : liste de projets non nulle");

			FieldLayoutManager flm = ComponentManager.getInstance().getFieldLayoutManager();

			Iterator projectIt = projectList.iterator();
			while (projectIt.hasNext()) {

				projectTemp = (Project) projectIt.next();

				IssueChildItem[] issueChildItems = mapping.getIssueChild().getIssueChildItem();
				for (i = 0; i < issueChildItems.length; i++) {
					if (issueChildItems[i].getParam().getKey().compareTo(LinkedIssueConstants.ID_PROJECT) == 0) {
						if ( issueChildItems[i].getParam().getValue().compareTo("CURRENT_PRJ") == 0) {
							issueChildProjectId = String.valueOf(parentIssue.getProjectObject().getId());
						}else{
							issueChildProjectId = issueChildItems[i].getParam().getValue();
						}

					}	
				}

				LOG.debug("ICI : projet en cours : " + projectTemp.getName());
				LOG.debug("ICI : projet en cours id : " + projectTemp.getId());
				LOG.debug("ICI : issue child project id : " + issueChildProjectId);

				if (String.valueOf(projectTemp.getId()).compareTo(issueChildProjectId) == 0) {

					LOG.debug("ICI : projet trouv� !");

					for (i = 0; i < issueChildItems.length; i++) {

						LOG.debug("ICI : param�tre issue child : " + issueChildItems[i].getParam().getKey());

						if (issueChildItems[i].getParam().getKey().compareTo(LinkedIssueConstants.ID_TYPE) == 0) {

							LOG.debug("ICI : issue type trouv�");

							String issueTypeId = issueChildItems[i].getParam().getValue();
							FieldLayout fl = flm.getFieldLayout(projectTemp.getGenericValue(),issueTypeId);


							LOG.debug("ICI : field layout r�cup�r� pour ce projet/issueType");

							LnioFieldsMappingConfiguration lnioFieldsConfig = null;
							try {
								lnioFieldsConfig = lnioConfigManager.getLnioFieldsMappingConfigurationObject();
							} catch (LinkNewIssueOperationException e) {
								e.printStackTrace();
								LOG.error("Failed to load LNIO configuration");
							}

							LnioContextBean lcb = (LnioContextBean) lnioFieldsConfig.getConfiguredContexts().get(String.valueOf(mapping.getId()));
							List fieldsMapping = lcb.getListOfFieldsMapping();

							for (Object fieldLayoutItemObject : fl.getFieldLayoutItems()) {
								FieldLayoutItem fieldLayoutItem = (FieldLayoutItem) fieldLayoutItemObject;
								if(fieldLayoutItem.isRequired())
								{
									OrderableField reqField = fieldLayoutItem.getOrderableField();

									LOG.debug("ICI : id du champ requis : " + reqField.getId());

									if ( (reqField.getId().compareTo(IssueFieldConstants.SUMMARY) != 0)
											&& (reqField.getId().compareTo(IssueFieldConstants.REPORTER) != 0)
											&& (reqField.getId().compareTo(IssueFieldConstants.ISSUE_TYPE) != 0) ) {

										LOG.debug("ICI : champ requis : " + reqField.getName());

										if (fieldsMapping != null) {

											Iterator it = fieldsMapping.iterator();

											while (it.hasNext()) {

												fieldMappingTemp = (LnioMappingOfFieldBean) it.next();

												LOG.debug("ICI : liste de projets non nulle");

												if (reqField.getId().compareTo(fieldMappingTemp.getDestinationFieldId())  == 0 ) {
													fieldMappingFound = true;
												}

											}

											if (!fieldMappingFound) {
												return true;
											}

										}else{
											return true;
										}
									}
								}

							}
						}
					}


				}


			}




		}else{
			LOG.debug("There's no existing projects in JIRA");
		}



		return false;

	}





}

package com.valiantys.jira.plugins.createissueandlink.util;

public enum FieldsMappingExecutionContext {

    CREATION,
    SYNCHRONIZATION;
}

package com.valiantys.jira.plugins.createissueandlink.business;

import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueChild;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueChildItem;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping;
import com.valiantys.jira.plugins.createissueandlink.util.LinkedIssueConstants;

public class MappingWrapper {

	Mapping mapping;
	
	public MappingWrapper(Mapping mapping) {
		super();
		this.mapping = mapping;
	}

	public Mapping getMapping() {
		return mapping;
	}

	public void setMapping(Mapping mapping) {
		this.mapping = mapping;
	}
	
	/**
     * Function testing if the issueChild if the issue child of the mapping is entirely defined
     * (entirely defined) = issue type defined && deistination project defined && link description defined  
     * @return
     */
    public boolean isEntirelydChildDefined() {
    	
    	// retrieval of child mapping child issue definition
    	IssueChild issueChild = this.mapping.getIssueChild();
    	
    	int defineLevel = 0;
    	String valueConfig = "";
    	String keyConfig = "";
    	
    	if (this != null) {
    		
    		IssueChildItem[] issueChildItems = issueChild.getIssueChildItem();
    		
    		if ((issueChildItems != null) && (issueChildItems.length > 0)) {
    			
    			for (int i = 0; i < issueChildItems.length; i++) {
    				
    				valueConfig = issueChildItems[i].getParam().getValue();
    				keyConfig = issueChildItems[i].getParam().getKey();
    				
    				// if this parameter is the issue type, or the destination project, or the link definition
    				if ((keyConfig.compareTo(LinkedIssueConstants.ID_TYPE) == 0)
    						|| (keyConfig.compareTo(LinkedIssueConstants.ID_PROJECT) == 0)
    						|| (keyConfig.compareTo(LinkedIssueConstants.LINK_DESCRIPTION) == 0)) {
    					
    					// if this value is defined
    					if (valueConfig.compareTo("") != 0) {
    						
    						defineLevel++;
    						
    					}
    					
    				}
    				
    			}
    			
    		}
    		
    	}
    	
    	// if the defineLevel equals 3,
    	if (defineLevel == 3) {
    		return true;
    	}else{
    		return false;
    	}
    	
    }
	
	
}

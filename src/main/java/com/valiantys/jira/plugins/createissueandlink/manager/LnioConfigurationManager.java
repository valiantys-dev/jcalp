package com.valiantys.jira.plugins.createissueandlink.manager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.valiantys.jira.plugins.createissueandlink.LinkNewIssueOperationException;
import com.valiantys.jira.plugins.createissueandlink.LinkedIssueContextManager;
import com.valiantys.jira.plugins.createissueandlink.business.LnioContextBean;
import com.valiantys.jira.plugins.createissueandlink.business.LnioFieldsMappingConfiguration;
import com.valiantys.jira.plugins.createissueandlink.business.LnioSynchronizationConfiguration;
import com.valiantys.jira.plugins.createissueandlink.business.SynchronizationConfigBean;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueChild;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueChildItem;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueParent;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueParentItem;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.LinkedIssueContext;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem;
import com.valiantys.jira.plugins.createissueandlink.util.LinkedIssueConstants;

/**
 * 
 * @author Maxime
 * @since v 1.4.0
 */
public class LnioConfigurationManager implements ILnioConfigurationManager {

	/**
	 * The logger.
	 */
	protected final static Logger LOG = Logger.getLogger(LnioConfigurationManager.class);

	/**
	 * The transformer.<br>
	 * This attribute is used to marshall and unmarshall
	 * LNIOFieldMappingConfiguration.
	 */
	private XStream transformer = null;

	private ObjectMapper mapper;

	/**
	 * Hold the current configuration.
	 */
	private LnioFieldsMappingConfiguration lnioFieldsMapConfig = null;

	/**
	 * Hold the current configuration for synchro.
	 */
	private LnioSynchronizationConfiguration lnioSynchroConfig = null;

	/**
	 * Hold all properties, used to store global.
	 */
	private PluginSettings pluginSettings;

	/**
	 * Factory SAL pour creation de settings
	 */
	private final PluginSettingsFactory pluginSettingsFactory;

	/**
	 * All contexts defined in the xml configuration file.
	 */
	private LinkedIssueContext linkedIssueContext;

	/**
	 * JIRA Home
	 */
	private JiraHome jiraHome;

	/**
	 * Constructor.
	 * 
	 * @param lnioProperties
	 *            : ILNIOProperties.
	 */
	public LnioConfigurationManager(JiraHome jiraHome, final PluginSettingsFactory pluginSettingsFactory) {
		this.jiraHome = jiraHome;
		this.pluginSettingsFactory = pluginSettingsFactory;
	}

	private void initTransformer() {
		if (transformer == null) {
			mapper = new ObjectMapper();
			transformer = new XStream(new DomDriver("UTF-8"));
			transformer.setMode(XStream.ID_REFERENCES);
		}
	}

	/**
	 * Get LnioFieldsMappingConfiguration Object configuration from properties.
	 * 
	 * @return LnioFieldsMappingConfiguration configuration object.
	 * @throws LinkNewIssueOperationException
	 */
	public final LnioFieldsMappingConfiguration getLnioFieldsMappingConfigurationObject() throws LinkNewIssueOperationException {

		String lnioConfigXml = null;
		if (lnioFieldsMapConfig == null) {
			if (pluginSettings == null) {
				pluginSettings = pluginSettingsFactory.createGlobalSettings();
			}

			if (pluginSettings.get(LinkedIssueConstants.LNIO_CONFIG_XML) != null) {
				lnioConfigXml = String.valueOf(pluginSettings.get(LinkedIssueConstants.LNIO_CONFIG_XML));
			}

			if (lnioConfigXml != null) {
				initTransformer();
				lnioFieldsMapConfig = (LnioFieldsMappingConfiguration) transformer.fromXML(lnioConfigXml);
			} else {
				lnioFieldsMapConfig = createADefaultCoherentConfig();
			}
		}
		return lnioFieldsMapConfig;
	}

	/**
	 * Create of default coherente configuration.
	 * 
	 * @throws LinkNewIssueOperationException
	 *             : LinkNewIssueOperationException.
	 */
	private LnioFieldsMappingConfiguration createADefaultCoherentConfig() throws LinkNewIssueOperationException {
		LnioFieldsMappingConfiguration lnioFieldsMapConfig = new LnioFieldsMappingConfiguration();
		lnioFieldsMapConfig.setConfiguredContexts(createADefaultHashMapCoherentConfig());
		return lnioFieldsMapConfig;
	}

	/**
	 * 
	 * @throws LinkNewIssueOperationException
	 *             : LinkNewIssueOperationException.
	 */
	private HashMap createADefaultHashMapCoherentConfig() throws LinkNewIssueOperationException {
		HashMap lnioContexts = null;

		MappingsItem[] contexts = LinkedIssueContextManager.getInstance().getAllMappings();
		if (contexts != null) {
			lnioContexts = new HashMap(contexts.length);
			LnioContextBean aLnioContextBean = null;
			for (int i = 0; i < contexts.length; i++) {
				Mapping context = contexts[i].getMapping();
				aLnioContextBean = new LnioContextBean();
				aLnioContextBean.setIdContext(String.valueOf(context.getId()));
				aLnioContextBean.setName(context.getName());
				aLnioContextBean.setLabel(context.getLabel());
				aLnioContextBean.setLink(LinkedIssueConstants.ACTION_EDIT_FIELDS_MAPPING + context.getId());

				IssueParent issueParent = context.getIssueParent();
				IssueChild issueChild = context.getIssueChild();

				if (issueParent != null) {
					if (issueParent.getIssueParentItemCount() > 0) {
						IssueParentItem[] issueParentItems = issueParent.getIssueParentItem();
						String keyParam;
						String valueParam;
						for (int j = 0; j < issueParentItems.length; j++) {
							keyParam = issueParentItems[j].getParam().getKey();
							valueParam = issueParentItems[j].getParam().getValue();
							if (keyParam.equals(LinkedIssueConstants.ID_PROJECT)) {
								aLnioContextBean.setParentIssueIdProject(valueParam);
							} else if (keyParam.equals(LinkedIssueConstants.ID_TYPE)) {
								aLnioContextBean.setParentIssueIdType(valueParam);
							}
						}
					}
				}

				if (issueChild != null) {
					if (issueChild.getIssueChildItemCount() > 0) {
						IssueChildItem[] issueChildItems = issueChild.getIssueChildItem();
						String keyParam;
						String valueParam;
						for (int j = 0; j < issueChildItems.length; j++) {
							keyParam = issueChildItems[j].getParam().getKey();
							valueParam = issueChildItems[j].getParam().getValue();
							if (keyParam.equals(LinkedIssueConstants.ID_PROJECT)) {
								aLnioContextBean.setChildIssueIdProject(valueParam);
							} else if (keyParam.equals(LinkedIssueConstants.ID_TYPE)) {
								aLnioContextBean.setChildIssueIdType(valueParam);
							}
						}
					}
				}
				lnioContexts.put(String.valueOf(context.getId()), aLnioContextBean);
			}
		}
		return lnioContexts;
	}

	/**
	 * Set the LnioFieldsMappingConfiguration Object configuration into
	 * properties.
	 * 
	 * @param lnioFieldsMapConf
	 *            : represents LnioFieldsMappingConfiguration configuration
	 *            object.
	 */
	public final void storeLnioConfigObject(final LnioFieldsMappingConfiguration lnioFieldsMapConf) {
		if (lnioFieldsMapConf != null) {
			initTransformer();
			final String xml = transformer.toXML(lnioFieldsMapConf);

			if (pluginSettings == null) {
				pluginSettings = pluginSettingsFactory.createGlobalSettings();
			}
			pluginSettings.put(LinkedIssueConstants.LNIO_CONFIG_XML, xml);
			lnioFieldsMapConfig = lnioFieldsMapConf;
			LOG.debug("A configuration has been set for the following Key : " + LinkedIssueConstants.LNIO_CONFIG_XML + " - GlobalSettings");
		}

	}

	/**
	 * Check and store LNIO configuration from XML format.<br>
	 * 
	 * @param xmlLnioConfig
	 *            the LNIOConfig in XML format.
	 * @return true if store is success, else false.
	 */
	public final boolean checkAndStoreLnioConfig(final String xmlLnioConfig) {
		boolean status = false;
		LnioFieldsMappingConfiguration config = null;

		// Check parameter.
		if (xmlLnioConfig == null) {
			throw new NullPointerException("checkAndStoreSlaConfig method, parameter is null value.");
		}

		try {
			initTransformer();
			config = (LnioFieldsMappingConfiguration) transformer.fromXML(xmlLnioConfig);
		} catch (final Exception e) {
			final StringBuffer msg = new StringBuffer();
			msg.append("Error when parsing xml data : ");
			msg.append(e.getMessage());
			LOG.debug(msg.toString());
		}

		// If configuration is successfully created.
		if (config != null) {

			// Store configuration.
			this.storeLnioConfigObject(config);
			status = true;
		}

		return status;
	}

	public LnioSynchronizationConfiguration getLnioSynchronisationConfigurationObject() throws LinkNewIssueOperationException {
		return getConfigOfSynchro(LinkedIssueConstants.CONFIG_SYNCHRO_FILE_NAME);
	}

	public boolean isProjectSynchronizable(Long projectId) throws LinkNewIssueOperationException {
		if (lnioSynchroConfig == null) {
			getLnioSynchronisationConfigurationObject();
		}
		for (SynchronizationConfigBean aSynchronizationConfigBean : lnioSynchroConfig.getConfiguredSynchros()) {
			if (aSynchronizationConfigBean.getProjectId() != null) {
				if (aSynchronizationConfigBean.getProjectId().equals(LinkedIssueConstants.ALL)) {
					LOG.debug("The Project " + projectId + " is synchronizable...");
					return true;
				}

				Long synchroPrjId = new Long(aSynchronizationConfigBean.getProjectId());
				if (projectId.equals(synchroPrjId)) {
					LOG.debug("The Project " + projectId + " is synchronizable...");
					return true;
				}
			}
		}
		LOG.debug("The Project " + projectId + " has no synchronization defined");
		return false;
	}

	public SynchronizationConfigBean getSynchronizationConfig(Long projectId, Long linkTypeId) throws LinkNewIssueOperationException {
		if (lnioSynchroConfig == null) {
			getLnioSynchronisationConfigurationObject();
		}

		for (SynchronizationConfigBean aSynchronizationConfigBean : lnioSynchroConfig.getConfiguredSynchros()) {
			if (aSynchronizationConfigBean.getProjectId() != null && aSynchronizationConfigBean.getLinkId() != null) {
				Long synchroPrjId = null;
				boolean allProject = false;
				if (aSynchronizationConfigBean.getProjectId().equalsIgnoreCase(LinkedIssueConstants.ALL)) {
					synchroPrjId = new Long(-1);
					allProject = true;
				} else {
					synchroPrjId = new Long(aSynchronizationConfigBean.getProjectId());
				}
				Long synchroLinkId = new Long(aSynchronizationConfigBean.getLinkId());
				if ((projectId.equals(synchroPrjId) || allProject) && linkTypeId.equals(synchroLinkId)) {
					LOG.debug("A configuration of synchronization is found...");
					return aSynchronizationConfigBean;
				}
			}
		}
		LOG.debug("No configuration found");
		return null;
	}

	private LnioSynchronizationConfiguration getConfigOfSynchro(final String fileName) throws LinkNewIssueOperationException {

		if (lnioSynchroConfig != null) {
			return lnioSynchroConfig;
		} else {
			Reader reader = null;

			if (jiraHome != null && jiraHome.getHomePath() != null) {
				String jiraHomePath = jiraHome.getHomePath();
				StringBuffer sb = new StringBuffer();
				sb.append(jiraHomePath);
				sb.append(System.getProperty("file.separator"));
				sb.append(LinkedIssueConstants.CONFIG_PATH);
				sb.append(System.getProperty("file.separator"));
				sb.append(fileName);
				String customConfFilePath = sb.toString();

				// check if a custom configuration file exists in the
				// {JIRA_HOME} folder
				File fd = new File(customConfFilePath);
				if (fd.exists()) {
					// if it is, we load it
					try {
						reader = new BufferedReader(new InputStreamReader(new FileInputStream(customConfFilePath), Charset.forName("UTF-8")));
					} catch (FileNotFoundException e) {
						throw new LinkNewIssueOperationException(e);
					}
				}
			}

			if (reader == null) {
				// if it's not, we load the default configuration file
				// contained in the JAR file
				ClassLoader loader = LinkedIssueContextManager.class.getClassLoader();
				reader = new BufferedReader(new InputStreamReader(loader.getResourceAsStream(fileName), Charset.forName("UTF-8")));
			}

			if (reader != null) {
				lnioSynchroConfig = (LnioSynchronizationConfiguration) unserialize(reader);
				// LOG.debug("Conf synchro...");
				// for (SynchronizationConfigBean aSynchronizationConfigBean :
				// lnioSynchroConfig.getConfiguredSynchros()) {
				// LOG.debug("Field Mapping - "+
				// aSynchronizationConfigBean.getFieldsMappingId());
				// LOG.debug("Link Id - "+
				// aSynchronizationConfigBean.getLinkId());
				// LOG.debug("Project Id - "+
				// aSynchronizationConfigBean.getProjectId());
				// }
			}
		}
		return lnioSynchroConfig;
	}

	public Object unserialize(Reader reader) throws LinkNewIssueOperationException {
		LnioSynchronizationConfiguration object = null;
		try {
			object = (LnioSynchronizationConfiguration) mapper.readValue(reader, LnioSynchronizationConfiguration.class);
		} catch (Exception e) {
			throw new LinkNewIssueOperationException(e);
		}
		LOG.debug(" return object : " + object);

		return object;
	}

	/**
	 * Retrieve all context by parsing the xml configuration file.
	 * 
	 * @param confXmlFileName
	 *            : xml configuration file.
	 * @return all contexts in an object structure.
	 * @throws LinkNewIssueOperationException
	 *             : exception.
	 */
	public LinkedIssueContext getLinkedIssueContext(final String fileName) throws LinkNewIssueOperationException {

		LOG.debug("does linkedIssueContext be null : " + linkedIssueContext);
		if (linkedIssueContext != null) {
			return linkedIssueContext;
		} else {
			Reader reader = null;
			try {

				if (ComponentManager.getComponentInstanceOfType(JiraHome.class).getHomePath() != null) {
					String jiraHomePath = jiraHome.getHomePath();
					StringBuffer sb = new StringBuffer();
					sb.append(jiraHomePath);
					sb.append(System.getProperty("file.separator"));
					sb.append(LinkedIssueConstants.CONFIG_PATH);
					sb.append(System.getProperty("file.separator"));
					sb.append(fileName);

					// check if a custom configuration file exists in the
					// {JIRA_HOME} folder
					File fd = new File(sb.toString());

					if (fd.exists()) {

						// if it is, we load it
						reader = new BufferedReader(new InputStreamReader(new FileInputStream(sb.toString()), Charset.forName("UTF-8")));
					}
				}

				if (reader == null) {

					// if it's not, we load the default configuration file
					// contained in the JAR file
					ClassLoader loader = LinkedIssueContextManager.class.getClassLoader();
					reader = new BufferedReader(new InputStreamReader(loader.getResourceAsStream(fileName), Charset.forName("UTF-8")));

				}

				Unmarshaller unmarshaller = new Unmarshaller(LinkedIssueContext.class);
				linkedIssueContext = (LinkedIssueContext) unmarshaller.unmarshal(reader);
				reader.close();
			} catch (FileNotFoundException e) {
				LOG.error("Unable to find the configuration file : " + e.getMessage());
				throw new LinkNewIssueOperationException(e);
			} catch (MarshalException e) {
				LOG.error("The configuration file has a wrong format : " + e.getMessage());
				throw new LinkNewIssueOperationException(e);
			} catch (ValidationException e) {
				LOG.error("The configuration file is not valid : " + e.getMessage());
				throw new LinkNewIssueOperationException(e);
			} catch (IOException e) {
				LOG.error("LinkedIssueContextManager : IOException : " + e.getMessage());
				throw new LinkNewIssueOperationException(e);
			}
		}

		return linkedIssueContext;
	}

}

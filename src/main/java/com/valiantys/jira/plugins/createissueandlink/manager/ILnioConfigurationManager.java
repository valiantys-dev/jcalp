package com.valiantys.jira.plugins.createissueandlink.manager;

import com.valiantys.jira.plugins.createissueandlink.LinkNewIssueOperationException;
import com.valiantys.jira.plugins.createissueandlink.business.LnioFieldsMappingConfiguration;
import com.valiantys.jira.plugins.createissueandlink.business.LnioSynchronizationConfiguration;
import com.valiantys.jira.plugins.createissueandlink.business.SynchronizationConfigBean;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.LinkedIssueContext;

public interface ILnioConfigurationManager {

	/**
	 * Get LnioFieldsMappingConfiguration Object configuration from properties.
	 * 
	 * @return LnioFieldsMappingConfiguration configuration object.
	 * @throws LinkNewIssueOperationException 
	 */
	public LnioFieldsMappingConfiguration getLnioFieldsMappingConfigurationObject() throws LinkNewIssueOperationException;

	/**
	 * Set the LnioFieldsMappingConfiguration Object configuration into
	 * properties.
	 * 
	 * @param lnioFieldsMapConf
	 *            : represents LnioFieldsMappingConfiguration configuration
	 *            object.
	 */
	public void storeLnioConfigObject(final LnioFieldsMappingConfiguration lnioFieldsMapConf);

	/**
	 * Check and store LNIO configuration from XML format.<br>
	 * 
	 * @param xmlLnioConfig
	 *            the LNIOConfig in XML format.
	 * @return true if store is success, else false.
	 */
	public boolean checkAndStoreLnioConfig(final String xmlLnioConfig);
	
	public LnioSynchronizationConfiguration getLnioSynchronisationConfigurationObject() throws LinkNewIssueOperationException;

	public boolean isProjectSynchronizable(Long projectId) throws LinkNewIssueOperationException;
	
	public SynchronizationConfigBean getSynchronizationConfig(Long projectId, Long linkTypeId) throws LinkNewIssueOperationException;
	    
	public LinkedIssueContext getLinkedIssueContext(final String fileName) throws LinkNewIssueOperationException;
	
}

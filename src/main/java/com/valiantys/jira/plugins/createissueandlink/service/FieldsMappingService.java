package com.valiantys.jira.plugins.createissueandlink.service;

import java.util.Collection;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.util.IssueUpdateBean;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping;
import com.valiantys.jira.plugins.createissueandlink.util.FieldsMappingExecutionContext;

/**
 * Service used to syncrhonize fields between 2 issues.
 * @author Maxime Cojan
 *
 */
public interface FieldsMappingService {

    /**
     * Do a syncrhonization of fields for 2 issues.
     * @param mappingId
     * @param destinationId
     * @param originId
     * @param reverseMapping
     */
    public boolean doSynchronization(String mappingId, Long destinationId,  Long originId, boolean reverseMapping);
    
    /**
     * This function realise the field by field mapping between the parent issue
     * and the new child issue created
     * 
     * @param lnioConfigManager
     *            : the field by field mapping configuration object
     * @param destinationIssue
     *            : the new issue created
     * @param origineIssue
     *            : the parent issue
     * @param mapping
     *            : the mapping defined for the LNIO post function
     * @return
     */
    public Collection<ChangeItemBean> mapFieldsWithInheritValues(MutableIssue destinationIssue, Issue origineIssue, Mapping mapping, FieldsMappingExecutionContext executionContext, boolean reverseMapping);
//    public MutableIssue mapFieldsWithInheritValues(MutableIssue destinationIssue, Issue origineIssue, Mapping mapping, FieldsMappingExecutionContext executionContext, IssueUpdateBean issueUpdateBean, boolean reverseMapping);
}

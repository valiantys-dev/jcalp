package com.valiantys.jira.plugins.createissueandlink.listener;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.atlassian.jira.event.issue.AbstractIssueEventListener;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.issue.IssueEventListener;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.valiantys.jira.plugins.createissueandlink.LinkNewIssueOperationException;
import com.valiantys.jira.plugins.createissueandlink.service.SynchronizeIssuesService;
import com.valiantys.jira.plugins.createissueandlink.util.SynchronizationType;

/**
 * 
 * @author MCO [valiantys]
 * @since v1.0.0
 */
public class SynchronizeIssuesListener extends AbstractIssueEventListener implements IssueEventListener {

    /**
     * class Logger.
     */
    private static final Logger LOG = Logger.getLogger(SynchronizeIssuesListener.class);

    /**
     * Service of synchronization of issues
     */
    private final SynchronizeIssuesService synchronizeIssuesService;

    /**
     * Constructor.
     */
    public SynchronizeIssuesListener(final SynchronizeIssuesService synchronizeIssuesService) {
	this.synchronizeIssuesService = synchronizeIssuesService;

    }

    /**
     * {@inheritDoc}
     */
    public final void workflowEvent(final IssueEvent event) {

	if (event != null) {

	    // Retrieve the issue
	    Issue issue = event.getIssue();

	    List<Map<String, Object>> synchroTodo = null;
	    try {
		synchroTodo = synchronizeIssuesService.getAllSynchronizationsToDoByIssue(issue);
	    } catch (LinkNewIssueOperationException e) {
		e.printStackTrace();
	    }

	    if (synchroTodo != null && synchroTodo.size() > 0) {
		if (isEventTypeConcerned(event)) {
		    // case update fields
		    synchronizeIssuesService.launchASynchronizationOnThisIssue(issue, SynchronizationType.FIELDS, synchroTodo,event);
		}

		Comment aComment = event.getComment();

		if (aComment != null) {
		    // case comment
		    LOG.debug("case comment : " + aComment);
		    synchronizeIssuesService.launchASynchronizationOnThisIssue(issue, SynchronizationType.COMMENTS, synchroTodo, aComment, event);
		}
	    }
	}
    }

    /**
     * Check if the event type must launch an action to recalculate the
     * agreement Time.
     * 
     * @param event
     *            : event launched.
     * @return true if the action must be launch.
     */
    private boolean isEventTypeConcerned(final IssueEvent event) {
	if (event != null) {
	    Long eventType = event.getEventTypeId();
	    return !(eventType.equals(EventType.ISSUE_COMMENT_EDITED_ID) || eventType.equals(EventType.ISSUE_COMMENTED_ID) || eventType.equals(EventType.ISSUE_WORKLOG_DELETED_ID)
		    || eventType.equals(EventType.ISSUE_WORKLOG_UPDATED_ID) || eventType.equals(EventType.ISSUE_WORKLOGGED_ID));
	} else {
	    return false;
	}
    }

}

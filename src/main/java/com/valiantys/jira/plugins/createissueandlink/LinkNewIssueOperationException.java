package com.valiantys.jira.plugins.createissueandlink;

public class LinkNewIssueOperationException extends Exception {

	private Exception exception;
	
	public LinkNewIssueOperationException(Exception e) {
		exception = e;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}
	
}

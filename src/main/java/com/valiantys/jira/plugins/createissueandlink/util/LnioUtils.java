package com.valiantys.jira.plugins.createissueandlink.util;

import java.util.ArrayList;
import java.util.Collection;

import com.valiantys.jira.plugins.createissueandlink.business.LnioContextBean;

public class LnioUtils {

	/**
	 * Sort the list with the bubble algorithm.
	 * @param aList
	 * @return
	 */
	public static Collection bubbleSort(Collection aList) {
	    int n = aList.size();
	    boolean doMore = true;
	    
	    Object[] contextList = aList.toArray();
	    if(contextList != null)
	    while (doMore) {
	        n++;
	        
	        doMore = false;  // assume this is our last pass over the array
	        
	        LnioContextBean ctx0;
	        LnioContextBean ctx1;
	        LnioContextBean ctxTmp;
	        
	        
	        for (int i=0; i<contextList.length -1 ; i++) {
	        	ctx0 = (LnioContextBean)contextList[i];
	        	ctx1 = (LnioContextBean)contextList[i+1];
	        	if (ctx0.compareTo(ctx1) > -1) {
	                // exchange elements
	        		ctxTmp = ctx0;  
	        		contextList[i] = ctx1;
	        		contextList[i+1] = ctxTmp;
	            	doMore = true;  // after an exchange, must look again 
	            }
	        }
	    }
	   
	    aList = new ArrayList();
	    for (int i=0; i<contextList.length ; i++) {
	    	aList.add(contextList[i]);
	    }
	    
	    return aList;
	}
}

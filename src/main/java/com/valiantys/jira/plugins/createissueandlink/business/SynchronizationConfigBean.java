package com.valiantys.jira.plugins.createissueandlink.business;

public class SynchronizationConfigBean {
	
	/**
	 * Name of the field of the origin issue.
	 */
	private String projectId;

	/**
	 * Id of the field of the destination issue.
	 */
	private String linkId;

	/**
	 * Fields mapping IDs.
	 */
	private String fieldsMappingId;

	
	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getLinkId() {
		return linkId;
	}

	public void setLinkId(String linkId) {
		this.linkId = linkId;
	}

	public String getFieldsMappingId() {
		return fieldsMappingId;
	}

	public void setFieldsMappingId(String fieldsMappingId) {
		this.fieldsMappingId = fieldsMappingId;
	}
	
	
}

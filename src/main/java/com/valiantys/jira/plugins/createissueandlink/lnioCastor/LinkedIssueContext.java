/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3rc1</a>, using an XML
 * Schema.
 * $Id$
 */

package com.valiantys.jira.plugins.createissueandlink.lnioCastor;

/**
 * Class LinkedIssueContext.
 * 
 * @version $Revision$ $Date$
 */
public class LinkedIssueContext implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _inheritanceActivate.
     */
    private java.lang.String _inheritanceActivate;

    /**
     * Field _linkDisplayingActivate.
     */
    private java.lang.String _linkDisplayingActivate;

    /**
     * Field _multiLinkActivate.
     */
    private java.lang.String _multiLinkActivate;

    /**
     * Field _defaultLinkActivate.
     */
    private java.lang.String _defaultLinkActivate;

    /**
     * Field _mappings.
     */
    private com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mappings _mappings;


      //----------------/
     //- Constructors -/
    //----------------/

    public LinkedIssueContext() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'defaultLinkActivate'.
     * 
     * @return the value of field 'DefaultLinkActivate'.
     */
    public java.lang.String getDefaultLinkActivate(
    ) {
        return this._defaultLinkActivate;
    }

    /**
     * Returns the value of field 'inheritanceActivate'.
     * 
     * @return the value of field 'InheritanceActivate'.
     */
    public java.lang.String getInheritanceActivate(
    ) {
        return this._inheritanceActivate;
    }

    /**
     * Returns the value of field 'linkDisplayingActivate'.
     * 
     * @return the value of field 'LinkDisplayingActivate'.
     */
    public java.lang.String getLinkDisplayingActivate(
    ) {
        return this._linkDisplayingActivate;
    }

    /**
     * Returns the value of field 'mappings'.
     * 
     * @return the value of field 'Mappings'.
     */
    public com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mappings getMappings(
    ) {
        return this._mappings;
    }

    /**
     * Returns the value of field 'multiLinkActivate'.
     * 
     * @return the value of field 'MultiLinkActivate'.
     */
    public java.lang.String getMultiLinkActivate(
    ) {
        return this._multiLinkActivate;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid(
    ) {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(
            final java.io.Writer out)
    throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(
            final org.xml.sax.ContentHandler handler)
    throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'defaultLinkActivate'.
     * 
     * @param defaultLinkActivate the value of field
     * 'defaultLinkActivate'.
     */
    public void setDefaultLinkActivate(
            final java.lang.String defaultLinkActivate) {
        this._defaultLinkActivate = defaultLinkActivate;
    }

    /**
     * Sets the value of field 'inheritanceActivate'.
     * 
     * @param inheritanceActivate the value of field
     * 'inheritanceActivate'.
     */
    public void setInheritanceActivate(
            final java.lang.String inheritanceActivate) {
        this._inheritanceActivate = inheritanceActivate;
    }

    /**
     * Sets the value of field 'linkDisplayingActivate'.
     * 
     * @param linkDisplayingActivate the value of field
     * 'linkDisplayingActivate'.
     */
    public void setLinkDisplayingActivate(
            final java.lang.String linkDisplayingActivate) {
        this._linkDisplayingActivate = linkDisplayingActivate;
    }

    /**
     * Sets the value of field 'mappings'.
     * 
     * @param mappings the value of field 'mappings'.
     */
    public void setMappings(
            final com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mappings mappings) {
        this._mappings = mappings;
    }

    /**
     * Sets the value of field 'multiLinkActivate'.
     * 
     * @param multiLinkActivate the value of field
     * 'multiLinkActivate'.
     */
    public void setMultiLinkActivate(
            final java.lang.String multiLinkActivate) {
        this._multiLinkActivate = multiLinkActivate;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * com.valiantys.jira.plugins.createissueandlink.lnioCastor.LinkedIssueContext
     */
    public static com.valiantys.jira.plugins.createissueandlink.lnioCastor.LinkedIssueContext unmarshal(
            final java.io.Reader reader)
    throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (com.valiantys.jira.plugins.createissueandlink.lnioCastor.LinkedIssueContext) org.exolab.castor.xml.Unmarshaller.unmarshal(com.valiantys.jira.plugins.createissueandlink.lnioCastor.LinkedIssueContext.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate(
    )
    throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}
